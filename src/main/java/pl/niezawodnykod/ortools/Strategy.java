package pl.niezawodnykod.ortools;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public class Strategy {

    @Getter
    private final Map<String, Double> sectorToStockShare = new HashMap<>();

    public Strategy(Object... strategyParameters) {
        if (strategyParameters.length % 2 != 0) {
            throw new IllegalArgumentException("Provided strategy parameters are not properly defined: "
                    + strategyParameters.toString());
        }

        for (int index = 0; index < strategyParameters.length; index = index + 2) {
            sectorToStockShare.put(
                    (String) strategyParameters[index],
                    (double) strategyParameters[index + 1]
            );
        }
    }
}
