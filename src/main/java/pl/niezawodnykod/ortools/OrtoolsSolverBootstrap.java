package pl.niezawodnykod.ortools;

import com.google.ortools.Loader;
import com.google.ortools.linearsolver.MPConstraint;
import com.google.ortools.linearsolver.MPObjective;
import com.google.ortools.linearsolver.MPSolver;
import com.google.ortools.linearsolver.MPVariable;

import java.util.Map;
import java.util.Objects;

import static pl.niezawodnykod.ortools.Rating.*;

public class OrtoolsSolverBootstrap {

    private static final double INFINITY = java.lang.Double.POSITIVE_INFINITY;

    public static void main(String[] args) {
        Investor[] investors = new Investor[]{
                new Investor("peter", 100000, new Strategy("it", 0.3, "energy", 0.3, "finance", 0.3)),
                new Investor("bob", 100000, new Strategy("it", 0.6)),
                new Investor("john", 100000, new Strategy("finance", 0.5, "energy", 0.5)),
                new Investor("fred", 100000, new Strategy())
        };
        Stock[] stocks = new Stock[]{
                // energy
                new Stock("OXY", "Occidental Petroleum Corp", AAA, "energy", 22.39),
                new Stock("CXO", "Concho Resources Inc", AA, "energy", 65.60),
                new Stock("APA", "Apache Corp", B, "energy", 17.27),
                new Stock("COP", "Conocophillips", AA, "energy", 45.12),
                new Stock("NOV", "Nov Inc", BB, "energy", 14.75),
                // finance
                new Stock("AFL", "Aflac Inc", AA, "finance", 46.48),
                new Stock("AIG", "American International Group", AAA, "finance", 41.35),
                new Stock("AIZ", "Assurant Inc ", AA, "finance", 140.17),
                new Stock("AJG", "Arthur J. Gallagher & Company ", AAA, "finance", 116.11),
                new Stock("ALL", "Allstate Corp", A, "finance", 108.4),
                // it
                new Stock("ACN", "Accenture Plc", AAA, "it", 253.65),
                new Stock("ADBE", "Adobe Systems Inc", AAA, "it", 458.08),
                new Stock("AMD", "Adv Micro Devices", BB, "it", 88.21),
                new Stock("AKAM", "Akamai Technologies", AAA, "it", 106.45),
                new Stock("APH", "Amphenol Corp", BBB, "it", 131.74),
        };

        Loader.loadNativeLibraries();
        // GLOP - double
        // SCIP - int
        MPSolver solver = MPSolver.createSolver("SCIP");
        Solution solution = new Solution(investors, stocks);

        createAmountVariablesForEachStockPerInvestor(solver, solution);
        // create equations for each investor to limit the portfolio to available funds
        applyConstraintToLimitPortfolioToAvailableFunds(solver, solution);
        // create equations for each investor to include portfolio exposure
        applyConstraintToLimitMarketExposure(solver, solution);
        // create objective to maximize usage of highest quality stock
        applyObjectiveToMaximizeUsageOfHighestQualityAsset(solver, solution);

        MPSolver.ResultStatus result = solver.solve();
        System.out.println("The solver came with " + result.name() + " solution. It took "
                + solver.iterations() + " iterations within "
                + solver.wallTime() + " milliseconds."
        );
        solution.summary();
    }

    private static void applyObjectiveToMaximizeUsageOfHighestQualityAsset(MPSolver solver, Solution solution) {
        Investor[] investors = solution.getInvestors();
        Stock[] stocks = solution.getStocks();

        MPObjective objective = solver.objective();
        for (int i = 0; i < investors.length; i++) {
            for (int j = 0; j < stocks.length; j++) {
                Stock stock = stocks[j];
                MPVariable variable = solution.getVars()[i][j];
                double gainValue = stock.getRating().getFactor() * stock.getPrice();
                objective.setCoefficient(variable, gainValue);
            }
        }
        objective.setMaximization();
    }

    private static void createAmountVariablesForEachStockPerInvestor(MPSolver solver, Solution solution) {
        Investor[] investors = solution.getInvestors();
        Stock[] stocks = solution.getStocks();

        for (int i = 0; i < investors.length; i++) {
            for (int j = 0; j < stocks.length; j++) {
                Investor investor = investors[i];
                Stock stock = stocks[j];
                String name = investor.getName() + "-" + stock.getName();
                double amount = investor.getAvailableFunds() / stock.getPrice() * 0.25;
                solution.getVars()[i][j] = solver.makeIntVar(0.0, amount, name);
            }
        }
    }

    private static void applyConstraintToLimitMarketExposure(MPSolver solver, Solution solution) {
        Investor[] investors = solution.getInvestors();
        Stock[] stocks = solution.getStocks();

        for (int i = 0; i < investors.length; i++) {
            Investor investor = investors[i];
            Map<String, Double> sectorToStockShare = investor.getStrategy().getSectorToStockShare();
            for (Map.Entry<String, Double> entry : sectorToStockShare.entrySet()) {
                String sector = entry.getKey();
                String name = investor.getName() + "-" + sector;
                double limit = entry.getValue();
                double partOfFunds = investor.getAvailableFunds() * limit;
                MPConstraint constraint = solver.makeConstraint(partOfFunds, INFINITY, name);
                for (int j = 0; j < stocks.length; j++) {
                    Stock stock = stocks[j];
                    if (Objects.equals(sector, stock.getSector())) {
                        MPVariable variable = solution.getVars()[i][j];
                        constraint.setCoefficient(variable, stock.getPrice());
                    }
                }
            }
        }
    }

    private static void applyConstraintToLimitPortfolioToAvailableFunds(MPSolver solver, Solution solution) {
        Investor[] investors = solution.getInvestors();
        Stock[] stocks = solution.getStocks();

        for (int i = 0; i < investors.length; i++) {
            Investor investor = investors[i];
            MPConstraint constraint = solver.makeConstraint(-INFINITY, investor.getAvailableFunds(),
                    investor.getName() + "-available-funds");
            for (int j = 0; j < stocks.length; j++) {
                Stock stock = stocks[j];
                MPVariable variable = solution.getVars()[i][j];
                constraint.setCoefficient(variable, stock.getPrice());
            }
        }
    }
}
