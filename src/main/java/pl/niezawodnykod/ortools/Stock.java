package pl.niezawodnykod.ortools;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Stock {
    private final String ticker;
    private final String name;
    private final Rating rating;
    private final String sector;
    private final double price;
}
