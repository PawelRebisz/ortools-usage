package pl.niezawodnykod.ortools;

import com.google.ortools.linearsolver.MPVariable;
import lombok.Data;

import static java.lang.String.format;

@Data
public class Solution {

    private final MPVariable[][] vars;
    private final Stock[] stocks;
    private final Investor[] investors;

    public Solution(Investor[] investors, Stock[] stocks) {
        this.investors = investors;
        this.stocks = stocks;
        this.vars = new MPVariable[investors.length][stocks.length];
    }

    public void summary() {
        int padding = 12;

        System.out.print(leftPad("", padding));
        for (Stock stock : stocks) {
            System.out.print("|" + leftPad("'" + stock.getTicker() + "',r:" + stock.getRating().name(), padding));
        }
        System.out.println();

        System.out.print(leftPad("", padding));
        for (Stock stock : stocks) {
            System.out.print("|" + leftPad("p:" + format("%.2f", stock.getPrice()), padding));
        }
        System.out.println();

        System.out.print(leftPad("", padding));
        for (Stock stock : stocks) {
            System.out.print("|" + leftPad(stock.getSector(), padding));
        }
        System.out.println();

        System.out.print(leftPad("", padding).replaceAll(" ", "-"));
        for (Stock ignored : stocks) {
            System.out.print("|" + leftPad("", padding).replaceAll(" ", "-"));
        }
        System.out.println();

        for (int i = 0; i < investors.length; i++) {
            Investor investor = investors[i];
            System.out.print(leftPad(investor.getName(), padding));
            for (int j = 0; j < stocks.length; j++) {
                MPVariable variable = vars[i][j];
                System.out.print("|" + leftPad(format("%.2f", variable.solutionValue()), padding));
            }
            System.out.println();
        }
    }

    private static String leftPad(String content, int numberOfCharacters) {
        return format("%" + numberOfCharacters + "s", content);
    }
}
