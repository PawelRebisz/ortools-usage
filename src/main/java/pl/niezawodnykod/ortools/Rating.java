package pl.niezawodnykod.ortools;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Rating {
    AAA(1.),
    AA(.9),
    A(.85),
    BBB(.45),
    BB(.4),
    B(.3);

    @Getter
    private final double factor;
}
