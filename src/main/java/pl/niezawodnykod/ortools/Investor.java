package pl.niezawodnykod.ortools;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Investor {
    private final String name;
    private final double availableFunds;
    private final Strategy strategy;
}
